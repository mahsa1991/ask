<?php

use App\Image;
use Illuminate\Database\Seeder;

class ImageTableSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        Image::create([
            'name' => 'test.jpg',
        ]);
    }
}
