@extends('layout.master')
@section('content')
    <div class="row">
        <div class="col-12">

            <div class="col-sm-10">
                @foreach($images as $image)
                    <img src="/images/{{$image->name}}" class="img-responsive rounded float-left" :alt="img"/>
                @endforeach
            </div>

        </div>
    </div>

@endsection
