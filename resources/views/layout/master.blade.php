<!DOCTYPE html>
<html lang="fa" dir="rtl">
<head>
    <link type="text/css" rel="stylesheet" href="{{asset('assets/bootstrap-rtl.min.css')}}">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta charset="UTF-8">

    @yield('header')
    @yield('css')

</head>
<body>

    <div class="container">
        @yield('content')
    </div>

    @yield('varJs')
</body>
</html>