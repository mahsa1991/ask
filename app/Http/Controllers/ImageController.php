<?php

namespace App\Http\Controllers;

use App\Image;

class ImageController extends Controller
{
    public function index() {
        $images = Image::all();
        return view('show', compact('images'));
    }
}
