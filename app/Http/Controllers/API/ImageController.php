<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Resources\ImageResource;
use App\Image;

class ImageController extends Controller
{
    public function index() {
        return ImageResource::collection(Image::all());
    }
}
